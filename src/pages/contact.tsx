import React from "react"
import Navbar from "../components/Navbar"

const contact = () => {
  return (
    <div className="container">
      <Navbar />
      <div className="container font-sans mx-auto px-8 md:px-16 pt-4">
        <h2>Contact Me</h2>
        <span className='font-bold'>Email: </span>
        <p>juan.acosta7a@gmail.com</p>
        <span className='font-bold'>GitHub: </span>
        <p className='mb-0'>DonDyprax</p>
        <a href="https://github.com/DonDyprax">https://github.com/DonDyprax</a>
        <br />
        <span className='font-bold'>GitLab: </span>
        <p className='mb-0'>DonDyprax</p>
        <a href="https://gitlab.com/DonDyprax">https://gitlab.com/DonDyprax</a>
      </div>
    </div>
  )
}

export default contact
