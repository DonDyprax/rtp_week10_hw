import React from "react"
import Navbar from "../components/Navbar"

const home = () => {
  return (
    <div className="container">
      <Navbar />
      <div className="container mx-auto px-8 md:px-16 pt-4">
        <div className="aboutme font-sans">
          <h2 className="font-bold">Who am I?</h2>
          <p className=" text-justify">
            My name is Juan Pablo Acosta Valdivieso, I was born on March 10th
            1998 in El Salvador. Ever since I was a child, I have been attracted
            to computers and technology. Right after graduating from High
            School, I wasn't sure about what I wanted to study so I waited 6
            months after graduating High School before enrolling into the
            university. During those 6 months I kept wondering which career I
            should choose until I asked myself: Why not just study Computer
            Science? Luckily for me, a local university had just changed their
            Computer Science curriculum into a Computer Science Engineering
            curriculum which seemed more appealing to me. I ended up enrolling
            into the Computer Science Engineering career in the Central American
            University "Jose Simeon Cañas" and I'm currently coursing my 4th
            year.
          </p>
        </div>
        <div className="education font-sans">
          <h2 className="font-bold">Education</h2>
          <h4 className="px-4">Bachellor's Degree:</h4>
          <p className="text-bold px-4">Colegio Lamatepec - (2004 - 2016)</p>
          <h4 className="px-4">Higher Studies:</h4>
          <p className="text-bold px-4">
            Universidad Centroamericana 'Jose Simeon Cañas' - (2017 - Current)
          </p>
        </div>
        <div className="experience font-sans">
          <h2 className="font-bold">Experience</h2>
          <p>
            My work experience has been limited as of now. Most of my experience
            comes from projects done as part of university assignments. These
            projects range from developing simple programs displayed in the
            console, developing simple games, developing API's, developing
            mobile apps to developing web apps.
          </p>
        </div>
      </div>
    </div>
  )
}

export default home
