import React from 'react'
import Navbar from '../components/Navbar';
import ProjectList from '../components/ProjectList';

const projects = () => {
    return (
        <div className=''>
            <Navbar/>
            <div className='mx-auto px-8 md:px-16 pt-4 max-w-full'>
                <h2>Projects Page</h2>
                <ProjectList/>
            </div>
        </div>
    )
}

export default projects;