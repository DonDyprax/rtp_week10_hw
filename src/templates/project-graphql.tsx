import React from "react"
import { graphql } from "gatsby"
import Image from "gatsby-image"
import Navbar from "../components/Navbar"

export const query = graphql`
  query($slug: String!) {
    projectsJson(slug: { eq: $slug }) {
      title
      description
      stack
      deploy
      repo
      thumbnail {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`

const Project = ({ data }: any) => {
  const project = data.projectsJson

  return (
    <div className="project-detail-container container">
      <Navbar />
      <div className="project-detail font-sans container mx-auto px-8 md:px-16 pt-4 flex flex-col">
        <h1>{project.title}</h1>
        <Image
          fluid={project.thumbnail.childImageSharp.fluid}
          alt={project.title}
          className="w-auto"
        />
        <p className='mt-4'>{project.description}</p>
        <span className='font-bold'>Development Stack: </span>
        <div className="stack">
            {project.stack.map((tag: string) => {
                return <p>{tag}</p>
            })}
        </div>
        <span className='font-bold hover:text-blue'>Project Repository: </span>
        <a href={project.repo}>{project.repo}</a>
        <span className='mt-4 font-bold hover:text-blue'>Deployed Project: </span>
        <a className='mb-4' href={project.deploy}>{project.deploy}</a>
      </div>
    </div>
  )
}

export default Project
