import React from "react";
import { Link } from "gatsby";
import "@fontsource/lato";

const Navbar = () => {
  return (
    <nav className="box-border bg-gray-700 font-sans w-screen max-w-screen overflow-auto">
      <div className="mx-auto pr-8">
        <div className="flex justify-between">
          <div className="flex space-x-4">
            <div>
              <Link
                to="/"
                className="flex items-center py-5 px-2 text-white hover:text-gray-900"
              >
                <span className="font-bold">DonDyprax</span>
              </Link>
            </div>

            <div className="flex items-center space-x-1">
              <Link
                to="/"
                className="py-5 px-3 text-white hover:text-gray-900"
              >
                Home
              </Link>
              <Link
                to="/projects"
                className="py-5 px-3 text-white hover:text-gray-900"
              >
                Projects
              </Link>
              <Link
                to="/contact"
                className="py-5 px-3 text-white hover:text-gray-900"
              >
                Contact
              </Link>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
