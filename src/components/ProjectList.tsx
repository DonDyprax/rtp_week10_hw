import React, { useState } from "react"
import { Link, useStaticQuery, graphql } from "gatsby"
import Image from "gatsby-image"

interface ProjectType {
  title: string
  slug: string
  description: string
  thumbnail: any
}

const filters = ['js', 'ts', 'react', 'html', 'css', 'testing', 'all'];

const ProjectList = () => {
  const data = useStaticQuery(graphql`
    query {
      allProjectsJson {
        edges {
          node {
            title
            description
            slug
            stack
            thumbnail {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `)

  const [projectData, setProjectData] = useState(data.allProjectsJson.edges);
  const [filteredData, setFilteredData] = useState(data.allProjectsJson.edges);

  const filterByTag = (filter: string) => {
    let filteredArray: any = [];

    if(filter === 'all') {
      setFilteredData(projectData);
      return;
    }

    for(let i = 0; i < filteredData.length; i++) {
        if(filteredData[i].node.stack.includes(filter)) {
          filteredArray.push(filteredData[i]);
        }
    }

    setFilteredData(filteredArray);
  }

  return (
    <div className='container '>
      <span className='font-sans font-bold'>Filter by stack: </span>
      {filters.map((filter: string, index: number) => {
        return (
          <button
            onClick={() => filterByTag(filter)}
            key={index}
            className="bg-gray-700 active:bg-blue-700  hover:bg-blue-400 font-sans text-white py-2 px-4 rounded-full ml-4"
          >
            {filter}
          </button>
        )
      })}
      <div className="project-list mt-4 grid grid-cols-1 justify-items-center md:grid-cols-3 sm:grid-cols-2 lg:grid-cols-4">
        {filteredData.map((edge: any, index: number) => {
          const project = edge.node;

          return (
            <Link key={index} to={`/project/${project.slug}`}>
              <div className="project-card hover:text-indigo-500  text-center rounded-b bg-gray-700 text-white flex flex-col mb-10">
                <Image
                  fluid={project.thumbnail.childImageSharp.fluid}
                  alt={project.title}
                />
                <h2 className="mb-0 font-bold font-sans mt-2">{project.title}</h2>
              </div>
            </Link>
          )
        })}
      </div>
    </div>
  )
}
export default ProjectList
